# Tetrospective
## Description

My team asked me to host a retrospective for one time and I didn't want to do it the regular way. I was searching for some cool ideas to prevent a lame retrospective and I found **Tetrospective**! The idea came from: https://www.halfmoonagile.com/blog/the-tetrospective

The original idea was based on doing the retro physical and team members needed to close their eyes to get a Tetrospective piece. To solve that I've created this small python program that randomly choose a piece to pick and a team member. The script never choose the same team member twice in a row.

I've created a digital version of the Tetrospective pieces and a board in Mural.

## Visuals
* A cool Asciinema introduction video
<a href="https://asciinema.org/a/DF67oHx5BtwatZpGlE0xKWZTd" target="_blank"><img src="https://asciinema.org/a/DF67oHx5BtwatZpGlE0xKWZTd.svg" /></a>

* The **Tetrospective** board example as I used it on Mural
![Tetrospective Mural Board example](tetrospective_mural.jpg "Tetrospective Mural Board")

## Installation
1. Clone the repository
2. Create a virtual environment `python3 -m venv .env`
3. Activate the virtual environment `source .env/bin/activate`
4. Install dependencies `pip install -r ./requirements.txt`

## Usage
Just run the application when you're done with the installation steps `./tetrospective.py`

**Happy Tetro'ing!!**

## Support
If you need help, just create an issue in the repository.

## License
[Apache-2.0](LICENSE)
