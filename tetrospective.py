#!/usr/bin/env python3
from lib import clearscreen, pause
import random
import time
from art import tprint, text2art

def banner():
    clearscreen.clear()
    banner1 = text2art("Tetro",font='block',chr_ignore=True)
    banner2 = text2art("Spective",font='block',chr_ignore=True)
    print(banner1)
    print(banner2)

def color():
    colors = ["Green", "Orange", "Red", "Aqua", "Yellow", "Blue"]
    random_colors = random.sample(colors, 1)
    color = ''.join(random_colors)
    return color

def team_member():
    team = ["Maikel", "Jeroen", "Ewan", "Loek", "Ralph", "Kraai", "Robson", "Avishek", "Svrani"]
    random_team = random.sample(team, 1)
    team_member = ''.join(random_team)
    return team_member

def rolling(last_chosen_one):
    banner()
    answer = input("Lets play some Tetrospective! Roll now? (y/n) ")
    if answer == "y" or answer == "yes":
        chosen_team_member = team_member()
        chosen_one = chosen_team_member
        chosen_color = color()
        if chosen_one == last_chosen_one:
            rolling(last_chosen_one)
        else:
            clearscreen.clear()
            print("We've got color....")
            time.sleep(1)
            tprint(chosen_color)
            time.sleep(1)
            print("For....")
            time.sleep(1)
            tprint(chosen_one)
            last_chosen_one = chosen_one
            pause.user_input()
            rolling(last_chosen_one)
    elif answer == "n" or answer == "no":
        print("User said no!")
        exit(0)
    else:
        print("Wrong input! Try again!\n")
        pause.user_input()
        rolling(last_chosen_one)

def main():
    last_chosen_one = "first_run"
    rolling(last_chosen_one)

if __name__ == "__main__":
    main()
